/**
 * A Bot for Slack!
 */


/**
 * Define a function for initiating a conversation on installation
 * With custom integrations, we don't have a way to find out who installed us, so we can't message them :(
 */

function onInstallation(bot, installer) {
    if (installer) {
        bot.startPrivateConversation({user: installer}, function (err, convo) {
            if (err) {
                console.log(err);
            } else {
                convo.say('I am a bot that has just joined your team');
                convo.say('You must now /invite me to a channel so that I can be of use!');
            }
        });
    }
}


/**
 * Configure the persistence options
 */

var config = {};
if (process.env.MONGOLAB_URI) {
    var BotkitStorage = require('botkit-storage-mongo');
    config = {
        storage: BotkitStorage({mongoUri: process.env.MONGOLAB_URI}),
    };
} else {
    config = {
        json_file_store: ((process.env.TOKEN)?'./db_slack_bot_ci/':'./db_slack_bot_a/'), //use a different name if an app or CI
    };
}

/**
 * Are being run as an app or a custom integration? The initialization will differ, depending
 */

if (process.env.TOKEN || process.env.SLACK_TOKEN) {
    //Treat this as a custom integration
    var customIntegration = require('./lib/custom_integrations');
    var token = (process.env.TOKEN) ? process.env.TOKEN : process.env.SLACK_TOKEN;
    var controller = customIntegration.configure(token, config, onInstallation);
} else if (process.env.CLIENT_ID && process.env.CLIENT_SECRET && process.env.PORT) {
    //Treat this as an app
    var app = require('./lib/apps');
    var controller = app.configure(process.env.PORT, process.env.CLIENT_ID, process.env.CLIENT_SECRET, config, onInstallation);
} else {
    console.log('Error: If this is a custom integration, please specify TOKEN in the environment. If this is an app, please specify CLIENTID, CLIENTSECRET, and PORT in the environment');
    process.exit(1);
}


/**
 * A demonstration for how to handle websocket events. In this case, just log when we have and have not
 * been disconnected from the websocket. In the future, it would be super awesome to be able to specify
 * a reconnect policy, and do reconnections automatically. In the meantime, we aren't going to attempt reconnects,
 * WHICH IS A B0RKED WAY TO HANDLE BEING DISCONNECTED. So we need to fix this.
 *
 * TODO: fixed b0rked reconnect behavior
 */
// Handle events related to the websocket connection to Slack
controller.on('rtm_open', function (bot) {
    console.log('** The RTM api just connected!');
});

controller.on('rtm_close', function (bot) {
    console.log('** The RTM api just closed');
    // you may want to attempt to re-open
});


/**
 * Core bot logic goes here!
 */
// BEGIN EDITING HERE!

controller.on('bot_channel_join', function (bot, message) {
    bot.reply(message, "I'm here!")
});

controller.hears('hello', 'direct_message', function (bot, message) {
  console.log("in 'hello' function");
  bot.reply(message, 'Hello fam!');
  console.log("foo");
});

var _ = require('lodash');
var Spotify = require('node-spotify-api');
let spotify_id = process.env.SPOTIFY_ID;
let spotify_secret = process.env.SPOTIFY_SECRET;
var spotify = new Spotify({id: spotify_id, secret: spotify_secret});

// https://open.spotify.com/track/4b8Of9pmPK53OVWzvJZhbB?si=djEA5U_VRKmBoCAXB_aHvQ
controller.hears('https://open.spotify.com/track/.*?si=[^ ][^ ]*', 'ambient', function(bot, controller_message) {
  console.log("made it into the spotify listening hook");
  // "https://open.spotify.com/track/4b8Of9pmPK53OVWzvJZhbB?si=djEA5U_VRKmBoCAXB_aHvQ>"
  var spotify_url = controller_message.match[0];
  var spotify_track_id = spotify_url.match(/\/[A-Za-z0-9]+\?/)[0].substring(1, 23);
  console.log("spotify_url: ===" + spotify_url + "===");

  // 'https://api.spotify.com/v1/tracks/4b8Of9pmPK53OVWzvJZhbB'
  let built_url = 'https://api.spotify.com/v1/tracks/' + spotify_track_id;
  console.log(built_url);
  var result = spotify.request(built_url).
    then(
      function(data) {
        // https://www.youtube.com/results?sp=EgIgAQ%253D%253D&search_query=Gallant+-+Episode
        let reply_text =
          "https://www.youtube.com/results?sp=EgIgAQ%253D%253D&search_query="
          +
            (_.reduce(data['artists'], function(sum, e) { return sum = sum + " " + e['name'] }, '')
              + ' - '
              + data.name
            ).
            trim().
            replace(/ /g, '+');
        console.log(data);
        console.log("reply_text:");
        console.log(reply_text);
        console.log('end reply_text');
        bot.say({channel: controller_message.channel, text: reply_text});
        console.log(controller_message);
      },
      function(error) {
        console.log(error);
      }
    );
});
