# spottube

## The story of a (Slack)bot

A (not so) long time ago, a community called DC204 had a playlists channel in which Spotify links were shared.

Not everyone has Spotify, so links to songs from them were kind of useless to most people. Enter Spottube. It'll give you a link to Youtube search for the song.

## Using Spottube

Cloud: use heroku I guess as it's free. Click the button below to do it up.

**YOU MUST CLONE THIS PROJECT TO YOUR OWN GITHUB ACCOUNT FOR THIS BUTTON TO WORK. Heroku does not have an integration with GitLab. GitHub does and uses the referrer information to figure out what to deploy.** [![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

The bonus here is that you are ensuring that the bot's code remains in your control which protects you from losing this wonderful tool if I should delete this repository.

Locally: git clone this project; npm install; SPOTIFY_ID=zzz SPOTIFY_SECRET=zzz SLACK_TOKEN=zzz node index.js

What are those environment variables, you ask?

* SPOTIFY_ID - Your spotify username/id, also called the Client ID on their web page as of 2019-02-24, for accessing the Spotify API in order to extract the song information. It should be a random alphanumeric value in my experience.
* SPOTIFY_SECRET - Your spotify secret/password, also called the Client Secret on their web page as of 2019-02-24, for accessing the Spotify API in order to extract the song information. It should be a random alphanumeric value in my experience.
* SLACK_TOKEN - Your bot's secret token for authenticating against the Slack API. This requires you to use a Slack account. It can be your own account or a separate account you create to run the bot.

# Where's the code?

In index.js. Everything is crammed into there.

# What's this based on?

A bot tutorial from the creators of Slack for Slack.

The original project url:https://github.com/slackapi/easy-peasy-bot

You can find full instructions for building a bot app with this repository at https://medium.com/slack-developer-blog/easy-peasy-bots-getting-started-96b65e6049bf#.4ay2fjf32
